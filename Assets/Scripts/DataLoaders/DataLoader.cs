﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MemoryGame
{
    public class DataLoader : MonoBehaviour
    {
        [SerializeField] private DataSource _dataSource;

        public CardData[] LoadCardsData()
        {
            return _dataSource.GetCardsData();
        }

        public Sprite LoadCardsShirt()
        {
            return _dataSource.GetCardsShirt();
        }
    }
}