﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MemoryGame
{
    [RequireComponent(typeof(Text))]
    public class CounterPresenter : MonoBehaviour
    {
        private Text _counter;
        private void Awake()
        {
            _counter = GetComponent<Text>();
        }

        public void UpdateView(int count)
        {
            _counter.text = count.ToString();
        }
    }
}