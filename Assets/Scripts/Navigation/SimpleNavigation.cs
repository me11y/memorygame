﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MemoryGame
{
    public class SimpleNavigation : MonoBehaviour
    {
        [SerializeField] private SimpleScreen[] _screens;
        private int _currentScreenId = 0;
        public void Navigate(int id)
        {
            StartCoroutine(SwitchScreens(id));
        }

        private IEnumerator SwitchScreens(int id)
        {
            yield return StartCoroutine(_screens[_currentScreenId].DisableScreen());
            StartCoroutine(_screens[id].EnableScreen());
            _currentScreenId = id;
        }
    }
}