﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MemoryGame
{
    [RequireComponent(typeof(Canvas), typeof(GraphicRaycaster), typeof(CanvasGroup))]
    public class SimpleScreen : MonoBehaviour
    {
        private Canvas _canvas;
        private GraphicRaycaster _raycaster;
        private CanvasGroup _group;

        private void Awake()
        {
            _canvas = GetComponent<Canvas>();
            _raycaster = GetComponent<GraphicRaycaster>();
            _group = GetComponent<CanvasGroup>();
        }

        public void SetScreenActive(bool isActive)
        {
            _canvas.enabled = isActive;
            _raycaster.enabled = isActive;
        }

        public IEnumerator EnableScreen()
        {
            SetScreenActive(true);
            _group.DOFade(1, 0.3f);
            yield return new WaitForSeconds(0.3f);
        }

        public IEnumerator DisableScreen()
        {
            _group.DOFade(0, 0.3f).onComplete = () =>
            {
                SetScreenActive(false);
            };
            yield return new WaitForSeconds(0.3f);
        }
    }
}