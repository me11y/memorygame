﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace MemoryGame
{
    public class Preloader : MonoBehaviour
    {
        [SerializeField] private WebDataSource _dataSource;
        [SerializeField] private PreloaderPresenter _view;
        public UnityAction OnReadyToStart;

        private void OnEnable()
        {
            _dataSource.LoadingStarted += StartLoading;
            _dataSource.LoadingFinished += StopLoading;
        }

        private void OnDisable()
        {
            _dataSource.LoadingStarted -= StartLoading;
            _dataSource.LoadingFinished -= StopLoading;
        }

        private void StartLoading()
        {
            _view.StartLoading();
        }

        private void StopLoading()
        {
            _view.StopLoading();
            OnReadyToStart?.Invoke();
        }
    }
}