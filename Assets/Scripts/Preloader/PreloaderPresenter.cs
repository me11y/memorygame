﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MemoryGame
{
    public class PreloaderPresenter : MonoBehaviour
    {
        private Tween _animation;

        public void StartLoading()
        {
            _animation = transform.DORotate(new Vector3(0, 0, 180), 0.3f).SetLoops(-1, LoopType.Incremental);
            _animation.Play();
        }

        public void StopLoading()
        {
            _animation.Kill();          
        }
    }
}