﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace MemoryGame
{
    public class CardClickListener : MonoBehaviour, IPointerClickHandler
    {
        public int Id;
        public UnityAction<int> CardClicked;

        public void OnPointerClick(PointerEventData eventData)
        {
            CardClicked?.Invoke(Id);
        }
    }
}