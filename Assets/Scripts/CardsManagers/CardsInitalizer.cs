﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace MemoryGame
{
    public class CardsInitalizer : MonoBehaviour
    {
        [SerializeField] private DataLoader _dataLoader;
        private AnimatedCard[] _cards;

        private CardData[] _cardsData;
        private Sprite _cardsShirt;

        private void Awake()
        {
            _cards = FindObjectsOfType<AnimatedCard>();
        }

        public void Initialize()
        {
            GetData();
            InitCardsRandomly();
        }

        private void GetData()
        {
            _cardsData = _dataLoader.LoadCardsData();
            _cardsShirt = _dataLoader.LoadCardsShirt();
        }

        private void InitCardsRandomly()
        {
            MixCards();
            for (int i = 0; i < _cards.Length; i+=2)
            {
                _cards[i].Init(_cardsData[i / 2], _cardsShirt);
                _cards[i+1].Init(_cardsData[i / 2], _cardsShirt);
            }
        }

        private void MixCards()
        {
            for (int i = _cards.Length - 1; i >= 0; i--)
            {
                int j = Random.Range(0, i);
                var tmp = _cards[j];
                _cards[j] = _cards[i];
                _cards[i] = tmp;
            }
        }
    }
}