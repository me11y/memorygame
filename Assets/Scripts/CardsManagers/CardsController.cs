﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace MemoryGame
{
    public class CardsController : MonoBehaviour
    {
        [SerializeField] private Transform _cardsLayout;
        private AnimatedCard[] _cards;
        private readonly List<CardClickListener> _cardsClickListeners = new List<CardClickListener>();
        public UnityAction<int, int> OnCardSelected;

        private void Awake()
        {
            _cards = _cardsLayout.GetComponentsInChildren<AnimatedCard>();
            AddListeners();
        }

        private void AddListeners()
        {
            for(int i=0; i<_cards.Length; i++)
            {
                var listener = _cards[i].gameObject.AddComponent<CardClickListener>();
                listener.Id = i;
                _cardsClickListeners.Add(listener);
            }
        }

        private void OnEnable()
        {
            foreach(var cardListener in _cardsClickListeners)
            {
                cardListener.CardClicked += InvokeCardSelected;
            }
        }

        private void OnDisable()
        {
            foreach (var cardListener in _cardsClickListeners)
            {
                cardListener.CardClicked -= InvokeCardSelected;
            }
        }

        private void InvokeCardSelected(int id)
        {
            OnCardSelected?.Invoke(id, _cards[id].Data.Kind);
        }

        public void HideCards()
        {
            foreach(var card in _cards)
            {
                card.HideCard();
            }
        }

        public void ShowCard(int id)
        {
            _cards[id].ShowCard();
        }

        public void HideCard(int id)
        {
            _cards[id].HideCard();
        }

        public void RemoveCard(int id)
        {
            _cards[id].RemoveCard();
        }

        public int Count => _cards.Length;
    }
}