﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MemoryGame
{
    public class GameController : MonoBehaviour
    {
        [SerializeField] private CardsController _cards;
        [SerializeField] private float _cardsShowingTime;
        [SerializeField] private SimpleNavigation _navigation;

        private readonly Dictionary<int, int> _showedCards = new Dictionary<int, int>();
        private bool _canChoose = false;

        [SerializeField] private CounterPresenter _pairsCollectedPresenter;
        private int _pairsCollected = 0;

        private void OnEnable()
        {
            _cards.OnCardSelected += ChooseCard;
        }

        private void OnDisable()
        {
            _cards.OnCardSelected -= ChooseCard;
        }
        public void StartGame()
        {
            StartCoroutine(HideAllCards());
        }

        private IEnumerator HideAllCards() 
        {
            var waitForShowingTime = new WaitForSeconds(_cardsShowingTime);
            yield return waitForShowingTime;

            _cards.HideCards();
            _canChoose = true;
        }

        private void ChooseCard(int id, int kind)
        {
            if (_canChoose)
            {
                if (CanShowCard(id))
                {
                    _cards.ShowCard(id);
                    _showedCards.Add(id, kind);
                }
                StartCoroutine(CompareCardsIfChoosed());
            }
        }

        private bool CanShowCard(int id) => _showedCards.Count < 2 && !_showedCards.ContainsKey(id);

        private IEnumerator CompareCardsIfChoosed()
        {
            if (_showedCards.Count == 2)
            {
                _canChoose = false;

                List<int> kinds = new List<int>();
                foreach (var card in _showedCards)
                {
                    kinds.Add(card.Value);
                }

                if (kinds[0] == kinds[1])
                {
                    yield return StartCoroutine(CollectCards());
                }
                else
                {
                    yield return StartCoroutine(HideShowedCards());
                }

                _showedCards.Clear();
                _canChoose = true;

                GameOverIfAllCardsCollected();
            }
        }

        private IEnumerator HideShowedCards()
        {
            var waitForShowingTime = new WaitForSeconds(1);
            yield return waitForShowingTime;

            foreach (var card in _showedCards)
            {
                _cards.HideCard(card.Key);
            }
        }

        private IEnumerator CollectCards()
        {
            var waitForShowingTime = new WaitForSeconds(1);
            yield return waitForShowingTime;
            foreach (var card in _showedCards)
            {
                _cards.RemoveCard(card.Key);
            }
            _pairsCollected++;
            _pairsCollectedPresenter.UpdateView(_pairsCollected);
        }

        private void GameOverIfAllCardsCollected()
        {
            if (_pairsCollected == _cards.Count / 2)
            {
                _pairsCollected = 0;
                _pairsCollectedPresenter.UpdateView(_pairsCollected);
                _canChoose = false;
                _navigation.Navigate(0);
            }
        }
    }
}