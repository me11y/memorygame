﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MemoryGame {
    public class DataSource : MonoBehaviour
    {
        [SerializeField] protected CardData[] _data;
        [SerializeField] protected Sprite _shirt;

        public virtual CardData[] GetCardsData()
        {
            return _data;
        }

        public virtual Sprite GetCardsShirt()
        {
            return _shirt;
        }
    }
}