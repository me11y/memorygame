﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using Newtonsoft.Json;

namespace MemoryGame
{
    public class CardServerData
    {
        [JsonProperty("card_kind")] public int Kind;
        [JsonProperty("imageURL")] public string ImageURL;
    }

    public class ServerData
    {
        [JsonProperty("cards")] public CardServerData[] Cards;
        [JsonProperty("shirtURL")] public string ShirtImageUrl;
    }

    public class WebDataSource : DataSource
    {
        public UnityAction LoadingStarted;
        public UnityAction LoadingFinished;

        [SerializeField] private string _url;
        private UnityAction<ServerData> RawDataLoaded;

        private void OnEnable()
        {
            RawDataLoaded += ParseRawData;
        }

        private void OnDisable()
        {
            RawDataLoaded -= ParseRawData;
        }

        public void LoadData()
        {
            LoadingStarted?.Invoke();
            StartCoroutine(GetDataFromWeb());
        }

        private IEnumerator GetDataFromWeb()
        {
            UnityWebRequest www = UnityWebRequest.Get(_url);
            yield return www.SendWebRequest();

            if (www.isNetworkError)
            {
                Debug.Log(www.error);
            }
            else
            {
                ServerData rawData = JsonConvert.DeserializeObject<ServerData>(www.downloadHandler.text);
                RawDataLoaded?.Invoke(rawData);
            }
        }

        private void ParseRawData(ServerData rawData)
        {
            void SetShirt(Texture2D texture)
            {
                _shirt = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100.0f);
            }
            UnityAction<Texture2D> SetShirtTexture = SetShirt;

            StartCoroutine(LoadTexture(rawData.ShirtImageUrl, SetShirtTexture));

            _data = new CardData[rawData.Cards.Length];
            List<string> urls = new List<string>();
            for (int i = 0; i < _data.Length; i++)
            {
                _data[i].Kind = rawData.Cards[i].Kind;
                urls.Add(rawData.Cards[i].ImageURL);
            }

            StartCoroutine(SetCardsTextures(urls.ToArray()));
        }


        private IEnumerator SetCardsTextures(string[] urls)
        {
            for (int i = 0; i < _data.Length; i++)
            {
                UnityAction<Texture2D> SetCardTexture = (texture) =>
                {
                    _data[i].CardImage = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100.0f);
                };

                yield return StartCoroutine(LoadTexture(urls[i], SetCardTexture));
            }
            LoadingFinished?.Invoke();
        }

        private IEnumerator LoadTexture(string textureUrl, UnityAction<Texture2D> OnTextureLoaded)
        {
            UnityWebRequest www = UnityWebRequestTexture.GetTexture(textureUrl);
            yield return www.SendWebRequest();

            if (www.isNetworkError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Texture2D texture = ((DownloadHandlerTexture) www.downloadHandler).texture;
                OnTextureLoaded(texture);
            }
        }
    }
}