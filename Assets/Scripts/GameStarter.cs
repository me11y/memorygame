﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MemoryGame {
    public class GameStarter : MonoBehaviour
    {
        [SerializeField] private Preloader _preloader;
        [SerializeField] private GameController _game;
        [SerializeField] private CardsInitalizer _initializer;
        [SerializeField] private SimpleNavigation _navigation;
        private const int GAME_SCREEN_ID = 2;
        private void OnEnable()
        {
            _preloader.OnReadyToStart += StartGame;
        }

        private void OnDisable()
        {
            _preloader.OnReadyToStart -= StartGame;
        }

        private void StartGame()
        {
            _initializer.Initialize();
            _navigation.Navigate(GAME_SCREEN_ID);
            _game.StartGame();
        }
    }
}