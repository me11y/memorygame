﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MemoryGame
{

    [RequireComponent(typeof(Card), typeof(CanvasGroup))]
    public class AnimatedCard : MonoBehaviour
    {
        private Card _card;
        private CanvasGroup _group;
        private void Awake()
        {
            _card = GetComponent<Card>();
            _group = GetComponent<CanvasGroup>();
        }

        public CardData Data => _card.Data;

        public void Init(CardData data, Sprite shirt)
        {
            _group.alpha = 1;
            _card.Init(data, shirt);   
        }

        public void HideCard()
        {
            transform.DORotate(new Vector3(0, 90, 0), 0.2f).onComplete = () => {
                _card.HideCard();
                transform.eulerAngles = new Vector3(0, -90, 0);
                transform.DORotate(new Vector3(0, 0, 0), 0.3f);
            };     
        }

        public void ShowCard()
        {
            transform.DORotate(new Vector3(0, 90, 0), 0.2f).onComplete = () => { 
                _card.ShowCard();
                transform.eulerAngles = new Vector3(0, -90, 0);
                transform.DORotate(new Vector3(0, 0, 0), 0.3f);
            };   
        }

        public void RemoveCard()
        {
            ChangeImageOpacity(0);
        }

        private void ChangeImageOpacity(float alpha)
        {
            _group.DOFade(alpha, 0.3f);        
        }

    }
}