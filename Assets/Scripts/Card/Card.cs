﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MemoryGame
{
    [System.Serializable]
    public struct CardData
    {
        public int Kind;
        public Sprite CardImage;
    }

    [RequireComponent(typeof(Image), typeof(AspectRatioFitter))]
    public class Card : MonoBehaviour
    {
        public CardData Data { get; private set; }
        private Sprite _shirt;
        private Image _currentImage;
        private AspectRatioFitter _aspectRatioFitter;

        private void Awake()
        {
            _currentImage = GetComponent<Image>();
            _aspectRatioFitter = GetComponent<AspectRatioFitter>();
        }
        public void Init(CardData data, Sprite shirt)
        {
            Data = data;
            _shirt = shirt;
            _aspectRatioFitter.aspectRatio = (float) shirt.texture.width / shirt.texture.height;
            ChangeImageOpacity(255);
            _currentImage.sprite = Data.CardImage;
            _currentImage.SetNativeSize();
        }

        public void HideCard()
        {
            _currentImage.sprite = _shirt;
        }

        public void ShowCard()
        {
            _currentImage.sprite = Data.CardImage;
        }

        public void RemoveCard()
        {
            ChangeImageOpacity(0);
        }

        public void ChangeImageOpacity(float alpha)
        {
            Color temp = _currentImage.color;
            temp.a = alpha;
            _currentImage.color = temp;
        }
    }
}